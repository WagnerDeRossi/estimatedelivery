import {Page, IonicApp, Modal, Platform, NavController, NavParams, ViewController} from 'ionic-angular';
import {Control, ControlGroup} from 'angular2/common';
import {DeliveryService} from '../../services/DeliveryService';
import {StatusService} from '../../services/StatusService';

@Page({
    templateUrl: 'build/pages/home/home.html',
    providers: [DeliveryService, StatusService]
})

export class HomePage {  
    
    private _deliveries;
            
    private _selectedStatus: number;
    private _openedDelivery;         
    private _deliveryService : DeliveryService;
    private _statusService : StatusService;    

    constructor(deliveryService: DeliveryService, statusService : StatusService, private nav: NavController) {
                       
        this._deliveryService = deliveryService;   
        this._statusService = statusService;                    
        this._selectedStatus = 0;
        this._openedDelivery = undefined;   
        
        this.getDeliveries();
    }

    getDeliveries() {
        
        this._deliveries = this._deliveryService.getAvailableDeliveries();
        return this._deliveries;
    }
       
    openModal(delivery) {
        
        this._openedDelivery = delivery;
        
        var modalParam = {
            delivery: delivery,
            deliveries: this._deliveries
        };
        
        let modal = Modal.create(ModalActions, modalParam);
        
        modal.onDismiss(data => {    
            if (data)
            { 
                this._selectedStatus = data;
                this._deliveryService.updateDeliveryStatus(this._openedDelivery, this._selectedStatus);
                this.getDeliveries();
            }
            else
                this._selectedStatus = 0;
        });
        
        this.nav.present(modal);
    }
    
    setSelectedStatus(status, deliveryId){
        
        
    }
}


@Page({
    templateUrl: 'build/pages/home/modalAction.html',
    providers: [StatusService]
})
class ModalActions {

    private _allStatus;
    private _availableStatus;
    private _selectedAction;
    private _selectButtonDisabled;
    private _selectNextButtonDisabled;
    private _deliveriesListVisible;
    private _statusListVisible;
    private _selectedDelivery;
    private _selectedNextDelivery;
    private _deliveries;
    private _statusService : StatusService;
    private _platform : Platform; 
    private _viewCtrl : ViewController;
    private _params : NavParams;

    constructor(platform: Platform, params: NavParams, viewCtrl: ViewController, statusService : StatusService) {
       
        this._params = params;
        this._platform = platform;
        this._viewCtrl = viewCtrl;
        this._statusService = statusService;
        this._selectedAction = 0;        
        this._selectedDelivery = params.data.delivery;
        this._deliveries = params.data.deliveries;

        this._selectButtonDisabled = true;
        this._selectNextButtonDisabled = true;
        this._deliveriesListVisible = false;
        this._statusListVisible = true;
        
        this._allStatus = this._statusService.getToModal();
        this.setAvailableStatus();
        this.setDeliveriesList();
    }

    changeNextDelivery(value){
        this._selectedNextDelivery = value;
        this._selectNextButtonDisabled = false;
    }

    changeStatus(value){
        this._selectedAction = value;
        this._selectButtonDisabled = false;
        
        if (this._selectedAction.type == 'Delivered' || this._selectedAction.type == 'Canceled' || this._selectedAction.type == 'NotDelivered')
            this._deliveriesListVisible = true;
        else {
            this._deliveriesListVisible = false;
            this._selectedNextDelivery = undefined;
        }
    }

    confirmModal(){        
        this._viewCtrl.dismiss(this._selectedAction);
    }

    // confirmStatus(){        
    //     this._deliveriesListVisible = true;
    // }

    dismiss() {
        this._viewCtrl.dismiss(null);
    }  
    
    setAvailableStatus(){
        
        var newList = [];        
        
        //Remove the Receive item
        for(var i=0; i < this._allStatus.length;i++){
            
            if (this._allStatus[i].type == 'Delivered' || this._allStatus[i].type == 'NotDelivered'){
                
                if (this._selectedDelivery.status.type != 'Active')
                    continue;   
            }
            else if (this._allStatus[i].type == 'Active' && this._selectedDelivery.status.type == 'Active')
                continue;
            
            newList.push(this._allStatus[i]);      
        }
        
        this._availableStatus = newList;
    }
    
    setDeliveriesList(){
        
        var newList = [];        
        
        //Remove the First item
        for(var i=0; i < this._params.data.deliveries.length;i++){
            
            if (i== 0)
                continue;
            
            newList.push(this._params.data.deliveries[i]);      
        }
        
        this._deliveries = newList;
    }
}

