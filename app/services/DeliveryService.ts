import {Injectable} from 'angular2/core';
import {StatusService} from './StatusService';
import {Delivery} from './Delivery';
import {DeliveryStatus} from './DeliveryStatus';

@Injectable()
export class DeliveryService {
    
    private _deliveries = [];
    private _statusService : StatusService;
    
    constructor(statusService : StatusService) {
        
        this._statusService = statusService;
        this.createDeliveries();
    }
    
    createDeliveries(){
              
       var del1 = new Delivery(1, 'Av Ipiranga 6000, Partenon', 'Porto Alegre', 'Pizzaria X', 2, this._statusService.getById(1) );
       var del2 = new Delivery(2, 'Av Ceara 321, Partenon', 'Porto Alegre', 'Pizzaria X', 1, this._statusService.getById(1));
       var del3 = new Delivery(3, 'Av Plinio Brasil Milano 125', 'Porto Alegre', 'Pizzaria X', 3, this._statusService.getById(1));          
        
       this._deliveries.push(del1);
       this._deliveries.push(del2);
       this._deliveries.push(del3); 
       
       this.orderDeliveries();
    }

    getAll() {
       return this._deliveries;
    }
    
    getAvailableDeliveries() {
       
       var newList = [];
        
       //Remove the Delivered and Canceled itens
       for(var i=0; i <this._deliveries.length;i++){
            
            if (this._deliveries[i].status.type != 'Delivered' && this._deliveries[i].status.type != 'Canceled'){
                newList.push(this._deliveries[i]);
            }            
       }
        
       return newList;
    }
     
    public getById(id) {
        
       for (var i=0;i < this._deliveries.length;i++){
           if (this._deliveries[i].id == id){               
               return this._deliveries[i].clone();               
           }
       }
        
       return null;
    }
                        
    orderDeliveries(){
        
        for(var i=0;i < this._deliveries.length;i++){
            for(var y=0;y < this._deliveries.length;y++){
                
                if (this._deliveries[y].order > this._deliveries[i].order){
                    
                    var aux = this._deliveries[i].clone();
                    this._deliveries[i] = this._deliveries[y].clone();
                    this._deliveries[y] = aux;
                }
            }
        }
    }
    
    activateDelivery(delivery, status){
        
        
    }
    
    updateDeliveryStatus(delivery, status) {
        
       for(var i=0; i < this._deliveries.length; i++){
            
            if (this._deliveries[i].id == delivery.id){
                this._deliveries[i].status = status.clone();
                break;
            }            
       }
    }
}