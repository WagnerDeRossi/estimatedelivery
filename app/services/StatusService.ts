import {Injectable} from 'angular2/core';
import {DeliveryStatus} from './DeliveryStatus';

@Injectable()
export class StatusService {
    
    private _status = [];
    
    constructor() {
        this.createStatus();
    }
    
    createStatus(){
              
       var statusReceived = new DeliveryStatus(1, 'Recebida', 'Receber','Received'); 
       var statusActive = new DeliveryStatus(2, 'Ativa', 'Ativar', 'Active');
       var statusCanceled = new DeliveryStatus(3, 'Cancelado', 'Cancelar', 'Canceled');
       var statusDelivered = new DeliveryStatus(4, 'Entregue', 'Entregar', 'Delivered');
       var statusNotDelivered = new DeliveryStatus(5, 'Não Entregue', 'Não Entregar', 'NotDelivered');
                 
       this._status.push(statusReceived);
       this._status.push(statusActive);
       this._status.push(statusCanceled); 
       this._status.push(statusDelivered);
       this._status.push(statusNotDelivered);
    }

    getAll() {
       
       return this._status;
    }
    
    public getById(id) {
        
       for (var i=0;i < this._status.length;i++){
           if (this._status[i].id == id){               
               return this._status[i].clone();               
           }
       }
        
       return null;
    }
    
    getToModal() {
       
       var newList = [];
        
       //Remove the Receive item
       for(var i=0; i <this._status.length;i++){
            
            if (this._status[i].type != 'Received'){
                newList.push(this._status[i]);
            }            
       }
        
       return newList;
    }
}