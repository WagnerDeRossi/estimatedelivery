import {Injectable} from 'angular2/core';

@Injectable()
export class DeliveryStatus {
    
    id: number;
    name: string;
    actionName: string;
    type: string;
    
    constructor(id: number, name : string, actionName: string, type: string) {
        this.id = id;
        this.name = name;
        this.actionName = actionName;
        this.type = type;
    }   
    
    clone(){
        
        var newItem = new DeliveryStatus(this.id,
                                         this.name,
                                         this.actionName,
                                         this.type);
                                   
        return newItem;
    }  
}

