import {DeliveryStatus} from './DeliveryStatus';

export class Delivery {
    
    id: number;
    address : string;
    city: string;
    customerName: string; 
    order: number;
    status: DeliveryStatus;
    
    constructor(id: number, address : string, city: string, customerName: string, order: number, status: DeliveryStatus) {
        this.id = id;
        this.address = address;
        this.city = city;
        this.customerName = customerName; 
        this.order = order;
        this.status = status;
    }   
    
    clone(){
        
        var newItem = new Delivery(this.id,
                                   this.address,
                                   this.city,
                                   this.customerName,
                                   this.order,
                                   this.status);
                                   
        return newItem;
    } 
}

